const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const dbConfig = require("./config/db.config");
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const index = require('./routes/index');
const products = require('./routes/products');
const categories = require('./routes/categories');
const seeder = require('./routes/seeder/products');
const stripe = require("stripe")('PRIVT_KEY');
var multer = require('multer');
var axios = require('axios')
const { createBrotliCompress } = require('zlib');
const ejs = require('ejs');
const request = require('request');
const app = express();
require('dotenv').config();
var corsOptions = {
  origin: "http://localhost:8080"
};
global.__basedir = __dirname;
// Устанавливаем место хранения
const storage = multer.diskStorage({
  destination: './public/uploads/',
  filename: function(req, file, cb){
    cb(null,file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  }
});

// Начинаем загрузку
const upload = multer({
  storage: storage,
  fileFilter: function(req, file, cb){
    checkFileType(file, cb);
  }
}).single('myImage');

// Проверяем тип файла
function checkFileType(file, cb){
// Разрешеные типы
  const filetypes = /jpeg|jpg|png|gif/;
  // Проверка типов
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  
  const mimetype = filetypes.test(file.mimetype);

  if(mimetype && extname){
    return cb(null,true);
  } else {
    cb('Error: Images Only!');
  }
}
// Запускаем приложение
app.set('view engine', 'ejs');
// папка для сохранения данных 
app.use(express.static('./public'));

app.get('/', (req, res) => res.render('index'));

app.post('/upload', (req, res) => {
  upload(req, res, (err) => {
    if(err){
      res.render('index', {
        msg: err
      });
    } else {
      if(req.file == undefined){
        res.render('index', {
          msg: 'Error: No File Selected!'
        });
      } else {
        res.render('index', {
          msg: 'File Uploaded!',
          file: `uploads/${req.file.filename}`
        });
      }
    }
  });
});

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const db = require("./models");
const Role = db.role;
const User= db.user;

db.mongoose
  .connect(`mongodb://${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Successfully connect to MongoDB.");
    initial();
  })
  .catch(err => {
    console.error("Connection error", err);
    process.exit();
  });

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to bezkoder application." });
});

// routes
require("./routes/auth.routes")(app);
require("./routes/user.routes")(app);
require("./routes/turorial.routes")(app);
// set port, listen for requests
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

  function initial() {
console.log("sss")
  request.post(
    'http://localhost:3000/api/auth/signup',
    { json: {   username:process.env.name,
    email:process.env.email,
    password:process.env.password,
    roles:["admin"] } },
    function (error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log(body);
        }
    }
);
  

  Role.estimatedDocumentCount((err, count) => {
    if (!err && count === 0) {
      new Role({
        name: "user"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'user' to roles collection");
      });

      new Role({
        name: "moderator"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'moderator' to roles collection");
      });

      new Role({
        name: "admin"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'admin' to roles collection");
      });
    }
  });
}
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

app.use('/', index);
app.use('/products', products);
app.use('/categories', categories);
app.use('/seeder', seeder);
app.post("/charge", (req, res, next) => {
  let amount = req.body.total*100;

  stripe.customers.create({
    email: req.body.stripeToken.email,
    source: req.body.stripeToken.id //stripeToken.id для оплаты
  })
    .then(customer =>
      stripe.charges.create({
        amount,
        description: "DBshop",
        currency: "usd",
        customer: customer.id
      }))
    .then(charge => res.json(req.body.stripeToken));
});
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});
// ловим 404 и пересылаем обработчику ошибок
app.use(function(req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// обработчик ошибок
app.use(function(err, req, res, next) {
// устанавливаем локальные переменные, только выдача ошибки в разработке
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

// отображаем страницу с ошибкой
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
