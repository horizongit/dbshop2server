const express = require('express');
const router = express.Router();
const faker = require('faker');
const Product = require('../../models/Product');
const Category = require('../../models/Category');
const { Router } = require('express');



router.get('/', function (req, res, next) {
    const categories = ["Phone", "NoteBook","Computers","The tablet"];
        let product = new Product({
            name : "Iphone",
            price : 400,
            category: "Phone",
            description : "Phone",
            image: "https://i.pcmag.com/imagery/reviews/03xdTO0Ka4H4KvEgtSPg4c2-12.1569479325.fit_lpad.size_625x365.jpg"
        })
        
        
        product.save();
    
    for (let i = 0; i < categories.length; i++) {
        let cat = new Category({
            title: categories[i]
        });
        cat.save();
    }
    res.redirect('/')
});

router.route('/products').post(function(req, res) {
    var new_product = new Product(req.body);
    new_product.save(function(err, product) {
      if (err) res.send(err);
      res.json(product);
    })
  });

  router.route('/products/:productId').delete(function(req, res) {
    Product.remove({
      _id: req.params.productId
    }, function(err, Product) {
      if (err)
        res.send(err);
      res.json({ message: 'Task successfully deleted' });
    });
  });

  router.route('/products/:productId').put(function(req, res) {
    Product.findOneAndUpdate({_id:req.params.productId}, req.body, {new: true}, function(err, task) {
      if (err)
        res.send(err);
      res.json(task);
    });
  });


module.exports = router;