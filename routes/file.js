
const express = require("express");
module.exports = app => {
  const controller = require("../controllers/file.controller");

  var router = require("express").Router();

  router.post("/upload", controller.upload);
  router.get("/files", controller.getListFiles);
  router.get("/files/:name", controller.download)
};