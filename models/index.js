const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const dbConfig = require("../config/db.config.js");
const db = {};

db.mongoose = mongoose;

db.user = require("./user.model");
db.role = require("./role.model");

db.ROLES = ["user", "admin", "moderator"];
db.url = dbConfig.url;
db.tutorials = require("./tutorial.model.js")(mongoose);
module.exports = db;